<!-- You can erase any parts of this template not applicable/known to your Issue. -->

### Describe Your Environment

[Versions from your environment]

- App version:
- OS:
- Python:

[If used, please inform their versions]

- NumPy:
- NMRGlue:
- SciPy:
- H5Py:
- PyQtGraph:
- PYQTGRAPH_QT_LIB:
- PyOpenGL
- PySide:
- PyQt:

### Language

[Python] or [C]

### Description / Steps to Reproduce [if necessary]

[Description of the issue]

1. [First Step]
2. [Second Step]
3. [and so on...]

### Actual Result

[A description, output ou image of the actual result]

### Expected Results / Proposed Result

[A description, output ou image of the expected/proposed result]

### Relevant Code [if necessary]

[A piece of code to reproduce and/or fix this issue]

```
# code here to reproduce the problem
```