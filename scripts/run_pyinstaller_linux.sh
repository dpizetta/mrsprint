python get_version.py

NAME='mrsprint'
SYSTEM='linux'
VERSION=$(head -n 1 'got_version.temp')
DATE=`date +%Y%m%d%H%M`
EXECUTABLE=$NAME-$SYSTEM-portable-v$VERSION-build.$DATE
$EXECUTABLE

rm -rfv 'got_version.temp'

cd ..

rm -rfv `find -iname "out.log*"`
rm -rfv `find -iname "*.spec"`
rm -rfv `find -iname "*pycache*"`
rm -rfv `find -iname "*.pyc"`
rm -rfv ./build
rm -rfv ./dist

pyinstaller mrsprint/__main__.py \
	--noconfirm \
	--clean \
	--log-level=INFO \
    --specpath=./dist \
    --hidden-import=appdirs \
    --hidden-import=itertools \
    --hidden-import=packaging \
    --hidden-import=packaging.version \
    --hidden-import=packaging.specifiers \
    --hidden-import=packaging.requirements \
    --hidden-import=h5py \
	--name=$EXECUTABLE -F  \


echo "Creating MD5 Checksum ..."
md5sum ./dist/$EXECUTABLE > ./dist/checksum.md5

echo "Creating list of imported library versions ..."
pip freeze > ./dist/pip_lib_versions.info
conda list -e > ./dist/conda_lib_versions.info

echo "Compressing files ..."

cd ./dist
tar -cf - "$EXECUTABLE" "checksum.md5" "pip_lib_versions.info" "conda_lib_versions.info" | gzip > "$EXECUTABLE.tar.gz"
cd ../scripts

echo "Process finished ..."

