History
========

A brief history of this universe
--------------------------------

Being short, when difficulties arises at understanding magnetic
resonance phenomena, we should search tools that could help us.

At that time we have found nice explanations on
`Brian site <http://mrsrl.stanford.edu/~brian/bloch/>`_.

As we are fan of Python, we discovered this implementation from
`Neji <https://github.com/neji49/bloch-simulator-python>`_.

Yet, it was difficult to change parameters and create new graphics.
So, this work starts. From that, many ideas have shining, and, here we are.

The main developer/maintainer is the PhD in computational physics
Daniel C. Pizetta, at University of São Paulo, São Carlos, Brazil.

A lot of the code was developed by the bachelor, in physics and
biomolecular sciences, Victor H.M. Pessoa in his final paper.
He created many new functionalities, implemented
all file interactions and connections between graphical interface and
core code.

From now on, Clara Vidor is continuing the development, and we hope that
will still new features to be released soon.

We also have a head professor PhD Fernando Fernandes Paiva that provides
a technical support in magnetic resonance issues.
