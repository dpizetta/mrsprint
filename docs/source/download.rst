Downloads
=========

Here you will find documents and files to download.

Download binaries - click-and-run
---------------------------------
Binaries are for those do not wish to install any Python things.
We recommend them to the ones without any programming experience.
Download from links below.

* Portable Windows Binaries: coming soon!
* Portable Linux Binaries: coming soon!
* Portable Mac Binaries: coming soon!

Sou you can just download, decompress, click-and-run.

Documentation
-------------

Links for latest available documentation.

