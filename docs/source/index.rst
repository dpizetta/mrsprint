.. MR SPRINT documentation master file, created by
   sphinx-quickstart on Tue May  8 14:23:26 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MR SPRINT's documentation!
======================================

.. toctree::
   :maxdepth: 2

   readme.rst
   overview.rst
   history.rst
   modules.rst
   download.rst
   changes.rst
   authors.rst
   license.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
