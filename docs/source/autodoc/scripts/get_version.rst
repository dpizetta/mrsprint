get\_version module
===================

.. automodule:: get_version
    :members:
    :undoc-members:
    :show-inheritance:
