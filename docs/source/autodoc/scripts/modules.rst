scripts
=======

.. toctree::
   :maxdepth: 4

   generate_qrc
   get_version
   process_icons
   process_ui
