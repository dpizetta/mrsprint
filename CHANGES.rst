
Changelog
=========

v1.2
----

    * Add file explorer
    * Add about
    * Add docs to ReadTheDocs
    * Improved and new icons, and logo
    * Bug fixes

v1.1
----

    * Fix docs, examples

v1.0
----

    * First public API

v0.6
----

    * New structure, revised files and screenshots
    * Fix doc style
    * Change nucleous to nucleus
    * Fix pyqtgraph imports

v0.5
----

    * Add new extensions for HDF5 files for contexts
    * More examples of samples
    * Add simulation context
    * Bug fixes

v0.4
----

    * Add sample examples
    * Bug fixes
    * Improvements on 2D editor and 3D plot

v0.3
----

    * UI Improvements
    * Add git-lab CI
    * Add pylint
    * Open and save samples in HDF5
    * Bug fixes

v0.2
----

    * Removing unnecessary things
    * Improvements in code
    * Docs with Sphinx

v0.1
----

    * First version with binaries for Windows and Linux - Pyinstaller
    * Add tox
    * Add scripts for pyinstaller, ui, icons
    * 2D editor and 3D view
    * Main window and toolbars