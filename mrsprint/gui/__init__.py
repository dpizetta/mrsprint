#! python
# -*- coding: utf-8 -*-

"""Package for GUI related classes and objects.

Authors:
    * Victor Hugo de Mello Pessoa <victor.pessoa@usp.br>
    * Daniel Cosmo Pizetta <daniel.pizetta@usp.br>

Since:
    2017/01/09

"""
