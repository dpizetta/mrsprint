#! python
# -*- coding: utf-8 -*-

"""Package for subject related classes and objects.

Authors:
    * Daniel Cosmo Pizetta <daniel.pizetta@usp.br>

Since:
    2017/10/01

"""
