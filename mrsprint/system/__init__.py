#! python
# -*- coding: utf-8 -*-

"""Package for system related classes and objects.


Authors:
    * Daniel Cosmo Pizetta <daniel.pizetta@usp.br>

Since:
    2015/11/01

"""
